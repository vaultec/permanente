package io.permanente;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInput;
import java.io.DataOutput;
import java.io.File;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Base64;
import java.util.HashMap;

import org.apache.commons.io.FileUtils;
import org.mapdb.DataInput2;
import org.mapdb.DataOutput2;
import org.mapdb.Serializer;

import io.ipfs.api.IPFS;
import io.ipfs.api.MerkleNode;
import io.ipfs.api.NamedStreamable;
import io.permanente.primitives.IPFSVideo;
import io.permanente.primitives.VideoEntry;

/**
 * This is where *Some* (Most) of the action happens
 * NOTE: keep format exactly the same as Addvideotoqueue Entry format, it may not look good but keep it the same!
 * TODO: Add support for IPFSVideo format.
 */
public class Worker extends Thread {
	private VideoEntry Entry;
	private Database Database;
	private Config config;
	private IPFS ipfs;
	private Permanente permanente;
	
	public Worker(VideoEntry Entry, Config config, Database Database, IPFS ipfs, Permanente permanente) {
		this.Entry = Entry;
		this.config = config;
		this.Database = Database;
		this.ipfs = ipfs;
		this.permanente = permanente;
	}
	public VideoEntry GetEntry() {
		return this.Entry;
	}
	private void Download(File fd, String url) {
		
		try {
		    URL URL = new URL(url);
		    FileUtils.copyURLToFile(URL, fd);
		} catch (Exception e) {
		    System.err.println(e);
		    e.printStackTrace();
		}
	}
	/**
	 * HTTP Head request
	 * @param URL
	 * @return
	 */
	private boolean head(String URL) { 
		try { 
			HttpURLConnection.setFollowRedirects(false);
			HttpURLConnection con = (HttpURLConnection) new URL(URL).openConnection();
			con.setRequestMethod("HEAD");
			return (con.getResponseCode() == HttpURLConnection.HTTP_OK);
		} catch (Exception e ) {
			e.printStackTrace();
			return false;
		}
	}
	//Worker that actually does stuff
	@Override
	public void run() {
		int Max_Tries = 2;
		File VideoDir = this.config.Vid_Location;
		File vh240file;
		File vh480file;
		File vh720file;
		File vh1080file;
		if(Entry.misc.author != null && Entry.misc.permlink != null) {
			vh240file = new File(VideoDir + "/" +  Entry.misc.author + "@" + Entry.misc.permlink +"_vh240");
			vh480file = new File(VideoDir + "/" + Entry.misc.author  + "@" + Entry.misc.permlink +"_vh480");
			vh720file = new File(VideoDir + "/" + Entry.misc.author + "@" + Entry.misc.permlink +"_vh720");
			vh1080file = new File(VideoDir + "/" + Entry.misc.permlink + "@" + Entry.misc.permlink +"_vh1080");

		} else {
			vh240file = new File(VideoDir + "/" + Entry.ID +"_vh240");
			vh480file = new File(VideoDir + "/" + Entry.ID +"_vh480");
			vh720file = new File(VideoDir + "/" + Entry.ID +"_vh720");
			vh1080file = new File(VideoDir + "/" + Entry.ID +"_vh1080");
		}
		
		if(Entry.video != null) {
			
		}
		
		
		
		if(Entry.vh_240p != null) {
			for(int x = 0; x < Max_Tries; x++) {
				//Support for already IPFS videos. simple solution. (Replace with pin instead of download)
				if(this.head("https://video.dtube.top/ipfs/" + this.Entry.vh_240p)) {
					Download(vh240file, "https://video.dtube.top/ipfs/" + this.Entry.vh_240p);
					try {
						NamedStreamable.FileWrapper file = new NamedStreamable.FileWrapper(vh240file);
						MerkleNode addResult = ipfs.add(file).get(0);
						this.Entry.misc.v240p_size = this.ipfs.cat(addResult.hash).length;
						
						if(!Entry.vh_240p.toBase58().equals(addResult.hash.toBase58())) {
							System.out.println("[ERROR] Downloaded video hash does not match locally generated. retrying...");
							ipfs.pin.rm(addResult.hash); //That way server keeps garbage out 
							continue;
						} else {
							System.out.println(addResult.hash.toBase58());
							//I'm assuming that the hashes will ALWAYS be the same....
							//DataBaseEntry.put("vh240", addResult.hash.toBase58()); 
							//DataBaseEntry.put("vh240LA", 0L);
							Entry.misc.la_240p = 0L;
							break;
						}
					} catch (IOException e) {
						e.printStackTrace();
						System.out.println("An Error has occurred while adding video to IPFS");
					}
					vh240file.delete();
				} else {
					//Download(vh240file, "https://gateway.ipfs.io/ipfs/" + this.Entry.vh_240p);
					
					try { 
						this.ipfs.pin.add(this.Entry.vh_240p);
						this.Entry.misc.v240p_size = this.permanente.getsize(this.Entry.vh_240p);
					} catch(IOException e) {
						System.err.println("[ERROR] Failed to pin hash: " + this.Entry.vh_240p.toBase58());
						e.printStackTrace();
					}
					break;
				}
				
			}
		}
		if(Entry.vh_480p != null) {
			for(int x = 0; x < Max_Tries; x++) {
				//Support for already IPFS videos. simple solution. (Replace with pin instead of download)
				if(this.head("https://video.dtube.top/ipfs/" + this.Entry.vh_480p)) {
					Download(vh480file, "https://video.dtube.top/ipfs/" + this.Entry.vh_480p);
					try {
						NamedStreamable.FileWrapper file = new NamedStreamable.FileWrapper(vh480file);
						MerkleNode addResult = ipfs.add(file).get(0);
						this.Entry.misc.v480p_size = this.ipfs.cat(addResult.hash).length;
						if(!this.Entry.vh_480p.toBase58().equals(addResult.hash.toBase58())) {
							System.out.println("[ERROR] Downloaded video hash does not match locally generated. retrying...");
							ipfs.pin.rm(addResult.hash); //That way server keeps garbage out 
							continue;
						} else {
							System.out.println(addResult.hash.toBase58());
							//DataBaseEntry.put("vh480", addResult.hash.toBase58());
							//DataBaseEntry.put("vh480LA", 0L);
							Entry.misc.la_480p = 0L;
							break;
						}
					} catch (IOException e) {
						e.printStackTrace();
						System.out.println("An Error has occurred while adding video to IPFS");
					}
					vh480file.delete();
				} else {
					//Download(vh480file, "https://gateway.ipfs.io/ipfs/" + this.Entry.vh_480p);
					try { 
						this.ipfs.pin.add(this.Entry.vh_480p);
						this.Entry.misc.v480p_size = this.permanente.getsize(this.Entry.vh_480p);
					} catch(IOException e) {
						System.err.println("[ERROR] Failed to pin hash: " + this.Entry.vh_480p.toBase58());
						e.printStackTrace();
					}
					Entry.misc.la_480p = 0L;
					break;
				}
			}
		}
		if(Entry.vh_720p != null) {
			for(int x = 0; x < Max_Tries; x++) {
				//Support for already IPFS videos. simple solution. (Replace with pin instead of download)
				if(this.head("https://video.dtube.top/ipfs/" + this.Entry.vh_720p)) {
					Download(vh720file, "https://video.dtube.top/ipfs/" + this.Entry.vh_720p);
				} else {
					//Download(vh720file, "https://gateway.ipfs.io/ipfs/" + this.Entry.vh_720p);
					try { 
						this.ipfs.pin.add(this.Entry.vh_720p);
						this.Entry.misc.v720p_size = this.permanente.getsize(this.Entry.vh_720p);
					} catch(IOException e) {
						System.err.println("[ERROR] Failed to pin hash: " + this.Entry.vh_720p.toBase58());
						e.printStackTrace();
					}
					Entry.misc.la_720p = 0L;
					break;
				}
				try {
					NamedStreamable.FileWrapper file = new NamedStreamable.FileWrapper(vh720file);
					MerkleNode addResult = ipfs.add(file).get(0);
					this.Entry.misc.v720p_size = this.ipfs.cat(addResult.hash).length;
					if(!this.Entry.vh_720p.toBase58().equals(addResult.hash.toBase58())) {
						System.out.println("[ERROR] Downloaded video hash does not match locally generated. retrying...");
						ipfs.pin.rm(addResult.hash); //That way server keeps garbage out 
						continue;
					} else {
						System.out.println(addResult.hash.toBase58());
						//DataBaseEntry.put("vh720", addResult.hash.toBase58());
						//DataBaseEntry.put("vh720LA", 0L);
						Entry.misc.la_720p = 0L;
						break;
					}
					
				} catch (IOException e) {
					e.printStackTrace();
					System.out.println("An Error has occurred while adding video to IPFS");
				}
				vh720file.delete();
			}
		}
		if(Entry.vh_1080p != null) {
			for(int x = 0; x < Max_Tries; x++) {
				//Support for already IPFS videos. simple solution. (Replace with pin instead of download)
				if(this.head("https://video.dtube.top/ipfs/" + this.Entry.vh_1080p)) {
					Download(vh1080file, "https://video.dtube.top/ipfs/" + this.Entry.vh_1080p);
				} else {
					//Download(vh1080file, "https://gateway.ipfs.io/ipfs/" + this.Entry.vh_1080p);
					try { 
						this.ipfs.pin.add(this.Entry.vh_1080p);
						this.Entry.misc.v1080p_size = this.permanente.getsize(this.Entry.vh_1080p);
					} catch(IOException e) {
						System.err.println("[ERROR] Failed to pin hash: " + this.Entry.vh_1080p.toBase58());
						e.printStackTrace();
					}
					Entry.misc.la_1080p = 0L;
					break;
				}
				try {
					NamedStreamable.FileWrapper file = new NamedStreamable.FileWrapper(vh1080file);
					MerkleNode addResult = ipfs.add(file).get(0);
					this.Entry.misc.v1080p_size = this.ipfs.cat(addResult.hash).length;
					if(!this.Entry.vh_1080p.toBase58().equals(addResult.hash.toBase58())) {
						System.out.println("[ERROR] Downloaded video hash does not match locally generated. retrying...");
						ipfs.pin.rm(addResult.hash); //That way server keeps garbage out 
						continue;
					} else {
						System.out.println(addResult.hash.toBase58());
						//DataBaseEntry.put("vh1080", addResult.hash.toBase58());
						//DataBaseEntry.put("vh1080LA", 0L);
						Entry.misc.la_1080p = 0L;
						break;
					}
				} catch (IOException e) {
					e.printStackTrace();
					System.out.println("An Error has occurred while adding video to IPFS");
				}
				vh1080file.delete();
			}
		}
		//this.config.Database.put(Entry.getString("author") + "@" + Entry.getString("permlink"), DataBaseEntry);
		this.Entry.misc.size = Entry.misc.v240p_size + Entry.misc.v480p_size + Entry.misc.v720p_size + Entry.misc.v1080p_size;
		//Entry.video = new IPFSVideo();
		//Entry.type = VideoEntry.entryType.Legacy; //Version 
		this.Database.video_database.put(Long.toString(Entry.ID), Entry);
		this.Database.db().commit();
	}
}
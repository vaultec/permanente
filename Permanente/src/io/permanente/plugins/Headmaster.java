package io.permanente.plugins;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.TimeUnit;

import org.json.JSONObject;
import org.mapdb.DB;
import org.mapdb.HTreeMap;
import org.mapdb.Serializer;

import com.google.gson.Gson;

import io.ipfs.api.IPFS;
import io.ipfs.multihash.Multihash;
import io.permanente.Database;
import io.permanente.Permanente;
import io.permanente.primitives.VideoEntry;

public class Headmaster extends Thread {
	private final IPFS ipfs;
	private final Database database;
	private final Permanente permanente;
	private long timeout;
	
	public Stats stats;
	
	
	public Headmaster(IPFS ipfs, Database database, Permanente permanente) {
		this.ipfs = ipfs;
		this.database = database;
		this.permanente = permanente;
		this.stats = new Stats(
				database.db);
		this.timeout = permanente.config.plugins.headmaster.polling_delay;
	}
	
	public JSONObject get_index() {
		JSONObject index = new JSONObject();
		try {
			//response.put("index", this.permanente.Database.video_database.keySet());
			Iterator<String> it = this.permanente.Database.video_database.keySet().iterator();
			ArrayList<String> fullist = new ArrayList<String>();
			while(it.hasNext()) {
				Object tmp = it.next();
				VideoEntry e = (VideoEntry) this.permanente.Database.video_database.get(tmp);
				ArrayList<Multihash> list = this.permanente.dtube.hashList(e);
				for(Multihash a : list) {
					fullist.add(a.toBase58());
				}
			}
			index.put("index", fullist);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		index.put("sat", 
				this.permanente.config.plugins.headmaster.sat);
		try {
			index.put("avg_n", stats.avg_n());
		} catch(Exception e) {
			e.printStackTrace();
		}
		return index;
	}
	
	
	private Object RandomFromList(Collection list) {
		Random rand = new Random();
		return list.toArray()[rand.nextInt(list.size())];
	}
	private void log(String in) {
		System.out.println("[Headmaster] " + in);
	}
	/**
	 * 
	 * @param num
	 * @param list
	 * @return float
	 */
	public float poll(int num, List<Multihash> list) {
		if(list.size() == 0) {
			return 0;
		}
		int nodes = 0;
		for(int x = 0; x < num; x++ ) {
			try {
				List<Map<String, Object>> tmp = ipfs.dht.findprovs((Multihash) this.RandomFromList(list));
				for(Map<String, Object> e : tmp) {
					if((int) e.get("Type") == 4) {
						nodes = nodes + 1;
					}
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return (float) nodes/(float) num;
	}
	/**
	 * Get total size of detectable network
	 * @param sat;
	 * @param num_objects;
	 * @param avg_n; average number of nodes per object
	 * @return
	 */
	public int get_totaln(int sat, int num_objects, int avg_n) {
		return 0;
	}
	/**
	 * Get average amount of nodes per object.
	 * @param num
	 * @param whether to return average or full amount
	 * @return float
	 */
	public float poll(int num) {
		ArrayList<Multihash> fi = permanente.dtube.FullIndex();
		return poll(num, fi);
	}
	@Override
	public void run() {
		int num = 2;
		for( ; ; ) {
			log("Running poll operation with " + num + " objects");
			long time = System.currentTimeMillis();
			float tmp = this.poll(num);
			
			float tmp2 = (stats.avg_n()+tmp)/2;
			System.out.println("test " + tmp);
			System.out.println("test 2 " + tmp2);
			log("Completed poll ("+(System.currentTimeMillis()-time)+"ms) with average of " + tmp2 + " nodes per object");
			stats.avg_n(tmp2);
			
			try {
				TimeUnit.MILLISECONDS.sleep(timeout);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	public class Stats {
		private ConcurrentMap<String, Float> store;
		
		public Stats(DB db) {
			this.store = db.hashMap("hm_stats3").keySerializer(Serializer.STRING).valueSerializer(Serializer.FLOAT)
					.createOrOpen();
		}
		
		public void avg_n(float avg_n) {
			store.put("avg_n", Float.valueOf(avg_n));
			database.commit();
		}
		public float avg_n() {
			Float tmp = store.get("avg_n");
			if(tmp == null) {
				return 0F;
			}
			return tmp.floatValue();
		}
	}
}

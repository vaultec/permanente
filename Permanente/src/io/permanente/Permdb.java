package io.permanente;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.Collection;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.TimeUnit;

import org.mapdb.DB;
import org.mapdb.DB.HashMapMaker;
import org.mapdb.DBMaker;

import com.google.gson.Gson;

import io.ipfs.api.IPFS;
import io.ipfs.api.MerkleNode;
import io.ipfs.api.NamedStreamable;
import io.ipfs.multihash.Multihash;

/**
 * DO NOT USE EXPERIMENTAL 
 * 
 * @author vaultec
 */
@SuppressWarnings({"rawtypes","unchecked"})
public class Permdb {
	private DB db;
	protected ConcurrentMap map;
	private IPFS ipfs;
	
	/**
	 * Creates a database instant with given location and ipfs API multiaddress address
	 * @param database location
	 * @param ipfs API address 
	 */
	public Permdb(File file, String IPFS_HOST) {
		this(file, new IPFS(IPFS_HOST));
	}
	/**
	 * Creates a database instant with given location and ipfs instance
	 * @param database location
	 * @param ipfs instance
	 */
	public Permdb(File file, IPFS ipfs) {
		this.ipfs = ipfs;
		db = DBMaker.fileDB(file).checksumStoreEnable().closeOnJvmShutdown().make();
		map = db.hashMap("map").createOrOpen();
	}
	
	
	class DbMap implements Map {
		private Permdb permdb;
		private ConcurrentMap map;
		
		public DbMap(Permdb permdb, String name) {
			this.permdb = permdb;
			if(name.equals("index")) {
				throw new RuntimeException("Invalid name " + name);
			}
			this.map = this.permdb.db.hashMap(name).createOrOpen();
		}
		
		@Override
		public int size() {
			return map.size();
		}
		
		@Override
		public boolean isEmpty() {
			return map.isEmpty();
		}
		
		@Override
		public boolean containsKey(Object key) {
			// TODO Auto-generated method stub
			return false;
		}
		
		@Override
		public boolean containsValue(Object value) {
			// TODO Auto-generated method stub
			return false;
		}
		
		@Override
		public Object get(Object key) {
			String hash = map.get(key).toString();
			byte[] bytes;
			try {
				bytes = ipfs.cat(Multihash.fromBase58(hash));
			} catch (IOException e) {
				e.printStackTrace();
				return null;
			}
			Gson gson = new Gson();
			//System.out.println(new String(bytes));
			return gson.fromJson(new String(bytes), java.lang.Object.class);
		}
		
		@Override
		public Object put(Object key, Object value) {
			if(this.map.containsKey(key)) {
				try {
					Multihash mh = Multihash.fromBase58(map.get(key).toString());
					if(ipfs.pin.ls().containsKey(mh)) {
						ipfs.pin.rm(mh);
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			Gson gson = new Gson();
			NamedStreamable.ByteArrayWrapper Bytewrapper = 
					new NamedStreamable.ByteArrayWrapper(gson.toJson(value).getBytes());
			MerkleNode md;
			try {
				md = ipfs.add(Bytewrapper).get(0);
			} catch (IOException e) {
				e.printStackTrace();
				return null;
			}
			return map.put(key, md.hash.toBase58());
		}
		
		@Override
		public Object remove(Object key) {
			String hash = map.get(key).toString();
			try {
				ipfs.pin.rm(Multihash.fromBase58(hash));
			} catch (IOException e) {
				e.printStackTrace();
				return null;
			}
			return map.remove(key);
		}
		
		@Override
		public void putAll(Map m) {
			// TODO Auto-generated method stub
			
		}
		
		@Override
		public void clear() {
			for(Object e : map.keySet()) {
				if(map.get(e) instanceof String) {
					try {
						Multihash mh = Multihash.fromBase58(map.get(e).toString());
						if(ipfs.pin.ls().containsKey(mh)) {
							ipfs.pin.rm(mh);
						}
					} catch (IOException e1) {
						e1.printStackTrace();
					}
				}
				map.remove(e);
			}
		}
		
		@Override
		public Set<Object> keySet() {
			return map.keySet();
		}
		
		@Override
		public Collection<Object> values() {
			// TODO Auto-generated method stub
			return null;
		}
		
		@Override
		public Set<Object> entrySet() {
			// TODO Auto-generated method stub
			return null;
		}
		
		public Set<Multihash> HashIndex() {
			return map.entrySet();
		}
	}
	
	public static void main(String args[]) {
		DB diskdb = DBMaker.fileDB("/tmp/dat1").closeOnJvmShutdown().make();
		Permdb db = new Permdb(new File("/tmp/dat"),"/ip4/127.0.0.1/tcp/5001");
		ConcurrentMap map = diskdb.hashMap("map").createOrOpen();
		
		//db.put("hello", "world");
		//System.out.println(db.get("hello"));
		//System.out.println(db.keySet());
		//map.put("test", "142384189hd938hd928dh98");
		//System.out.println(map.keySet());
		//map.put("123", new File(""));
		String a = "";
		//map.remove("test");
		//db.getStore().compact();
		//db.clear();
		try {
			long start = System.currentTimeMillis();
			int x = 0;
			while( x < 1000 ) {
				//map.get("test");
				byte[] l = new byte[128];
				new Random().nextBytes(l);
				//db.put("test", l);
				//db.get("test");
				
				
				x=x+1;
			}
			long end = System.currentTimeMillis();
			System.out.println(Long.toString(end-start) + "Ms @" +Long.toString((end-start)/1000)+" per ms");
			
		} finally {
			
		}
		
		long start = System.currentTimeMillis();
		int x = 0;
		while( x < 1000 ) {
			//map.get("test");
			byte[] l = new byte[128];
			new Random().nextBytes(l);
			map.put("test", l);
			map.get("test");
			
			
			x=x+1;
		}
		long end = System.currentTimeMillis();
		System.out.println(Long.toString(end-start) + "Ms @" +Long.toString((end-start)/1000)+" per ms");
		
		
		
		//System.out.println(map.keySet());
		
		try {
			TimeUnit.HOURS.sleep(5);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}

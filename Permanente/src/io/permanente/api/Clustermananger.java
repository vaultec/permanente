package io.permanente.api;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.json.JSONObject;

import com.google.gson.JsonObject;

import io.ipfs.multihash.Multihash;
import io.permanente.Config;
import io.permanente.Permanente;
import io.permanente.plugins.Headmaster;
import io.permanente.primitives.Session;
import io.permanente.primitives.VideoEntry;


/**
 * TODO: Convert to wrapper/simple callable class that interacts solely inside java.
 * Then wrap with API method for interaction, error handling, etc.
 * 
 * TODO: Find better name for dynamic session manager thingy.
 * 
 * NOTE: For cluster networking ALL allocation is managed by server (THERE IS NOTHING ELSE TO IT)
 * 
 * @author vaultec
 * @since v0.2
 * @version v0.2.0 beta
 */
public class Clustermananger extends Thread {
	/* HEAD NOTES
	 * 
	 */
	
	public boolean started;
	private final Permanente permanente;
	public Headmaster headmaster;
	private long expirelength = 21600000;
	private long timeout = 120*1000; //Time out in ms for a connection to be invalidated. (After enough time since last ping)
	
	public Clustermananger(Permanente perm) {
		this.permanente = perm;
		if(perm.config.plugins.enabled.contains(Config.eplugins.discord_bot)) {
			this.headmaster = new Headmaster(perm.ipfs, perm.Database, perm);
		}
	}
	public void run() {
		this.started = true;
		if(permanente.config.plugins.enabled.contains(Config.eplugins.discord_bot)) {
			this.headmaster.start();
		}
		// Thread here
		for( ; ; ) {
			Iterator<String> it = this.permanente.HTTPsessions.keySet().iterator();
			while(it.hasNext()) {
				String e = it.next();
				Session session = this.permanente.HTTPsessions.get(e);
				if(!this.isvalid(e) | (session.timeping() > this.timeout)) {
					//it.remove();
					//System.out.println("[INFO] Session: " + e + " has expired or reached timeout length");
				}
			}
			
			try {
				TimeUnit.SECONDS.sleep(45);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	//Essentially create a new active session with the API (Tracking clients)
	//EXAMPLES: Throw authentication exception for clients that DO NOT have valid token. (Used for authentication purposes)
	//For authentication, when authentication is disabled, hand the client a random cookie, and assign permID (thats it). 
	//When enabled check token and client ID for validity and assign permID.
	//If the token is invalid throw exception.
	//(session tokens are different than API tokens, API tokens are going to be used for stateless API, video adding and such) 
	//session tokens are essentially the clients authentication token.
	//Remember permIDs are PERMANENT (NEVER CHANGE) keep that in mind when designing this.
	public Session createsession(authrequest request) throws AuthenticationException {
		if(request == null) {
			return null;
		}
		Session session = new Session(); //Annoying
		if(request.token != null) {
			//Do authentication stuff here
		}
		
		session.cookie = this.CreateSaltString(10);
		session.timestamp = System.currentTimeMillis();
		session.expirelength = this.expirelength;
		session.lastseen = System.currentTimeMillis();
		session.userid = request.userid;
		
		//System.out.println(session.cookie);
		
		this.permanente.HTTPsessions.put(session.cookie, session);
		return session;
	}
	//This is where throwing an exception to up stream code comes in handy.
	//Upstream code APIMethods catches error then returns to client appropriate error.
	//For example if the session is invalid throw error. When client recevies codes from its interaction point (ClientInterface)
	//It will throw the same exception to the up stream code triggering it to re authenticate.
	public JSONObject ping(JsonObject in, String cookie) throws InternalException {
		JSONObject response = new JSONObject();
		try {
			//response.put("index", this.permanente.Database.video_database.keySet());
			Iterator<String> it = this.permanente.Database.video_database.keySet().iterator();
			ArrayList<String> fullist = new ArrayList<String>();
			while(it.hasNext()) {
				Object tmp = it.next();
				VideoEntry e = (VideoEntry) this.permanente.Database.video_database.get(tmp);
				ArrayList<Multihash> list = this.permanente.dtube.hashList(e);
				for(Multihash a : list) {
					fullist.add(a.toBase58());
				}
			}
			response.put("index", fullist);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		response.put("sat", 
				this.permanente.config.plugins.headmaster.sat);
		try {
			response.put("avg_n", this.headmaster.stats.avg_n());
		} catch(Exception e) {
			e.printStackTrace();
		}
		return response;
	}
	/**
	 * Boolean return value for whether session is valid.
	 * 
	 * @param cookie
	 * @return
	 */
	public boolean isvalid(String cookie) {
		if(!this.permanente.HTTPsessions.containsKey(cookie)) {
			return false;
		}
		Session session = this.permanente.HTTPsessions.get(cookie);
		if(session.timestamp+session.expirelength > System.currentTimeMillis()) {
			return true;
		} else {
			return false;
		}
	}
	//Data types/Methods
	class authrequest {
		String userid;
		String token; //Authentication token
		long timestamp;
	}
	class AuthenticationException extends Exception 
	{ 
	    public AuthenticationException(String s) 
	    { 
	        // Call constructor of parent Exception 
	        super(s); 
	    } 
	}
	class InternalException extends Exception {
		public InternalException(String s) {
			super(s);
		}
	}
	private String CreateSaltString(int length) {
		//#Stolen from stackoverflow
		String SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
		StringBuilder salt = new StringBuilder();
		Random rnd = new Random();
		while (salt.length() < length) { // length of the random string.
			int index = (int) (rnd.nextFloat() * SALTCHARS.length());
			salt.append(SALTCHARS.charAt(index));
		}
		String saltStr = salt.toString();
		return saltStr;

	}
}
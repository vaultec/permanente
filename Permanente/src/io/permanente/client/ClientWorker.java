package io.permanente.client;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.TimeUnit;

import org.mapdb.HTreeMap.KeySet;

import io.ipfs.api.IPFS;
import io.ipfs.multihash.Multihash;
import io.permanente.primitives.VideoEntry;

public class ClientWorker extends Thread {
	private VideoEntry Entry;
	private IPFS ipfs;
	private Client client;
	private KeySet<Multihash> index;
	
	private ArrayList<Multihash> queue;

	public ClientWorker(ArrayList<Multihash> queue, KeySet<Multihash> index, Client client, IPFS ipfs) {
		this.queue = queue;
		this.ipfs = ipfs;
		this.client = client;
		this.index = index;
	}
	public void run() {
		for( ; ; ) {
			Iterator<Multihash> it = this.index.iterator();
			if(it.hasNext()) {
				Multihash h = it.next();
				try {
					ipfs.pin.add(h);
					System.out.println("[Debug] Pinning file " + h.toBase58());
				} catch (IOException e) {
					System.out.println("[ERROR] Failed to pin file.");
					e.printStackTrace();
				} finally {
					this.index.add(h);
				}
			}
			
			try {
				TimeUnit.MILLISECONDS.sleep(5);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			this.client.SaveData();
		}
	}
}

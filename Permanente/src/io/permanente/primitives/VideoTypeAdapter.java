package io.permanente.primitives;

import java.lang.reflect.Type;
import java.util.Map;

import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonParser;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import io.permanente.Database;

public class VideoTypeAdapter implements JsonDeserializer<VideoEntry>, JsonSerializer<VideoEntry> {

	@Override
	public VideoEntry deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
			throws JsonParseException {
		//JsonObject jsonObject = json.getAsJsonObject();
		//String dataType = jsonObject.get("NAME").getAsString();
		return null;
		//return context.deserialize(json, FieldToClass.get(dataType));
	}

	@Override
	public JsonElement serialize(VideoEntry src, Type typeOfSrc, JsonSerializationContext context) {
		JsonParser parser = new JsonParser();
		JsonObject o = parser.parse(new Gson().toJson(src)).getAsJsonObject();
		return o;
	}
}
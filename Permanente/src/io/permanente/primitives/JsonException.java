package io.permanente.primitives;

import org.json.JSONException;
import org.json.JSONObject;
/**
 * Defines all the error handling with JSON RPC/DAPI
 * 
 * TODO: Not ready for mainstream uage (May remove later)
 * designed for ClientInterface to use. (Upstream error handling)
 * 
 * @author vaultec
 * @since v0.2
 */
public interface JsonException {
	//-32601
	public class Method_Not_Found extends Exception {
		private static final long serialVersionUID = 2835618216816677252L;

		public Method_Not_Found(String s) {
			super(s);
		}
	}
	//-32603
	public class Internal_Server_Error extends Exception {
		private static final long serialVersionUID = 6496805301687889028L;

		public Internal_Server_Error(String s) {
			super(s);
		}
	}
	//-32600
	public class Invalid_Request extends Exception {
		private static final long serialVersionUID = 7887998007465562324L;

		public Invalid_Request(String s) {
			super(s);
		}
	}
	//-32001
	public class SessionExpire extends Exception {
		private static final long serialVersionUID = 7190668647373058436L;

		public SessionExpire(String s) {
			super(s);
		}
	}
	//-32002
	public class AuthenticationException extends Exception {
		private static final long serialVersionUID = -5522695848652468320L;

		public AuthenticationException(String s) {
			super(s);
		}
	}
	public static void ParseException(JSONObject error) throws Method_Not_Found, Internal_Server_Error, Invalid_Request, JSONException, SessionExpire {
		switch(error.getInt("code")) {
			case -32601:
				throw new Method_Not_Found(error.getString("message"));
			case -32603:
				throw new Internal_Server_Error(error.getString("message"));
			case -32600:
				throw new Invalid_Request(error.getString("message"));
			case -32001:
				throw new SessionExpire(error.getString("message"));
		}
	}
}

package io.permanente.primitives;

import io.ipfs.multihash.Multihash;

/**
 * Defines what a non stateless connection lease. (designed for
 * authentication and tracking of client connections)
 * Allows easy extendability. Authentication etc
 * Clients connect to a state based call back that gives instructions and ping information. 
 * 
 * When clients call to master. Master will give sets of videos to pin.
 * 
 * @author vaultec
 * @since v0.2
 */
public class Session {
	public boolean pubsub; //is connection reachable through pubsub/ip2cs
	
	
	
	
	public String cookie; //session cookie used for EVERYTHING, Except dynamic allocation... That is calculated differently
	public String userid; //The alias of the user connection. Client automatically selects random value.
	
	public byte[] permid; //16 Byte permID for client (used for dynamic allocation) Going to be used later on DONT mess with
	public Multihash id;
	
	public long timestamp; //UNIX EPOCH timestamp (in milies) of the time the user subscribed.
	public long expirelength; //Length in milies for the connection to expire. Connection can be invalidated before connection.
	public long lastseen; //UNIX EPOCH. Used in API record each time the client pings the server (If the ping is longer than timeout.. Invalidate session) Recommended around 5 minutes.
	
	boolean authenticated;
	
	public void updateping() {
		this.lastseen = System.currentTimeMillis();
	}
	public long timeping() {
		return (System.currentTimeMillis()-this.lastseen);
	}
}
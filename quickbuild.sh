#Prerequirements
sudo apt -y install maven default-jdk;

cd Permanente

#Compile this will take awhile
echo -e "Compiling... this may take awhile"
mvn clean compile assembly:single >>  /dev/null

#move to current directory
mv target/Permanente*.jar ../permanente.jar

echo "run 'java -jar permanente.jar' to begin"